#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
sudo rm -rf /home/ubuntu/ci_cd_demo_vivek/

# clone the repo again
#git clone https://gitlab.com/abhinavdhasmana/ci_cd_demo.git
git clone https://gitlab.com/vivek.pratap/ci_cd_demo_vivek.git

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
#source /home/ubuntu/.nvm/nvm.sh

# stop the previous pm2
#pm2 kill
#npm remove pm2 -g


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
#npm install pm2 -g
# starting pm2 daemon
#pm2 status

#installing redis mongodb,nginx.and python
sudo apt-get install build-essential redis-server mongodb nginx python

#cd /home/ubuntu/ci_cd_demo/src

#installing node js
echo "installing node js 7.x"
curl -sL https://deb.nodesource.com/setup_8.x | bash -
sudo apt-get install -y nodejs

#moving to targeted folder
cd /home/ubuntu/ci_cd_demo_vivek/src

#install npm packages
#echo "Running npm install"
sudo npm install

#Restart the node server
sudo node app.js
